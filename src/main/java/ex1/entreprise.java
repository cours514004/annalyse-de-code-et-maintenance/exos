package ex1;

import java.util.Date;

public class Entreprise {

    public static final int CAPITAL_MAX = 3000000;
    private int siret;
    private String nom;
    private String adresse;
    private Date dateCreation;

    public Entreprise(int siret, String nom, String adresse, Date dateCreation) {
        this.siret = siret;
        this.nom = nom;
        this.adresse = adresse;
        this.dateCreation = dateCreation;
    }

    public void afficherStatut() {
        // Implémentation de la logique d'affichage du statut
        System.out.println("SIRET : " + siret);
        System.out.println("Nom : " + nom);
        System.out.println("Adresse : " + adresse);
        System.out.println("Date de création : " + dateCreation);
    }

    // Getters et Setters pour chaque variable si nécessaire
    public int getSiret() {
        return siret;
    }

    public void setSiret(int siret) {
        this.siret = siret;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }
}
