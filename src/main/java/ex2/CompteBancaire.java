package ex2;

/**
 * Représente un compte bancaire.
 */
public abstract class CompteBancaire {

    protected double solde;

    /**
     * Ajoute un montant au solde du compte.
     *
     * @param montant le montant à ajouter
     */
    public void ajouterMontant(double montant) {
        solde += montant;
    }

    /**
     * Débite un montant du solde du compte selon les règles spécifiques au type de compte.
     *
     * @param montant le montant à débiter
     */
    public abstract void debiterMontant(double montant);

    // Getters et Setters communs
    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }
}

/**
 * Compte courant permettant un découvert.
 */
class CompteCourant extends CompteBancaire {
    private double decouvert;

    public CompteCourant(double solde, double decouvert) {
        super.solde = solde;
        this.decouvert = decouvert;
    }

    @Override
    public void debiterMontant(double montant) {
        if (solde - montant >= decouvert) {
            solde -= montant;
        }
    }

    public double getDecouvert() {
        return decouvert;
    }

    public void setDecouvert(double decouvert) {
        this.decouvert = decouvert;
    }
}

/**
 * Livret A avec un taux de rémunération.
 */
class LivretA extends CompteBancaire {
    private double tauxRemuneration;

    public LivretA(double solde, double tauxRemuneration) {
        super.solde = solde;
        this.tauxRemuneration = tauxRemuneration;
    }

    @Override
    public void debiterMontant(double montant) {
        if (solde - montant >= 0) {
            solde -= montant;
        }
    }

    public void appliquerRemuAnnuelle() {
        solde += solde * tauxRemuneration / 100;
    }

    public double getTauxRemuneration() {
        return tauxRemuneration;
    }

    public void setTauxRemuneration(double tauxRemuneration) {
        this.tauxRemuneration = tauxRemuneration;
    }
}
