package ex2;

/**
 * Représente un compte bancaire de type Livret A avec un taux de rémunération.
 */
public class LivretA extends CompteBancaire {

    private double tauxRemuneration;

    /**
     * Constructeur pour un Livret A.
     *
     * @param solde Le solde initial du compte.
     * @param tauxRemuneration Le taux de rémunération annuel du Livret A.
     */
    public LivretA(double solde, double tauxRemuneration) {
        super(solde);  // Appel au constructeur de la classe parente CompteBancaire
        this.tauxRemuneration = tauxRemuneration;
    }

    // Getters et Setters
    public double getTauxRemuneration() {
        return tauxRemuneration;
    }

    public void setTauxRemuneration(double tauxRemuneration) {
        this.tauxRemuneration = tauxRemuneration;
    }
}

