package ex4;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    private static final String DEFAULT_PATTERN = "dd/MM/yyyy HH:mm:ss";

    /**
     * Formate une date selon un modèle donné.
     *
     * @param pattern le modèle de formatage
     * @param date la date à formater
     * @return la date formatée en chaîne de caractères
     */
    public static String format(String pattern, Date date) {
        return new SimpleDateFormat(pattern).format(date);
    }

    /**
     * Formate une date selon le modèle par défaut "dd/MM/yyyy HH:mm:ss".
     *
     * @param date la date à formater
     * @return la date formatée en chaîne de caractères
     */
    public static String formatDefaut(Date date) {
        return format(DEFAULT_PATTERN, date);
    }
}