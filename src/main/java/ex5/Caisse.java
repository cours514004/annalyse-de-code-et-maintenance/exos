package ex5;

import java.util.ArrayList;
import java.util.List;

public class Caisse {

    private String nom;
    private List<Item> items;
    private int poidsMin;
    private int poidsMax;

    /**
     * Constructeur
     *
     * @param nom
     * @param poidsMin
     * @param poidsMax
     */
    public Caisse(String nom, int poidsMin, int poidsMax) {
        this.nom = nom;
        this.items = new ArrayList<>();
        this.poidsMin = poidsMin;
        this.poidsMax = poidsMax;
    }

    /**
     * Getter pour l'attribut nom
     *
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Setter pour l'attribut nom
     *
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Getter pour l'attribut items
     *
     * @return the items
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     * Setter pour l'attribut items
     *
     * @param items the items to set
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }

    /**
     * Vérifie si la caisse peut accepter l'item
     *
     * @param item
     * @return true si l'item peut être accepté, false sinon
     */
    public boolean accepte(Item item) {
        return item.getPoids() >= poidsMin && item.getPoids() < poidsMax;
    }

    /**
     * Ajoute un item à la caisse
     *
     * @param item
     */
    public void addItem(Item item) {
        this.items.add(item);
    }
}
